name = Simple2
description = A simple 3 column layout demonstrating mobile first and Susy2.
preview = preview.png
template = simple2-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[secondary_menu] = Secondary menu
regions[navigation]     = Navigation bar
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[slider]         = Slideshow
regions[ptitle]         = Page title
regions[content]        = Content
regions[sidebar_first]  = First sidebar
regions[sidebar_second] = Second sidebar
regions[footer_first]   = Footer first
regions[footer_second]  = Footer second
regions[footer_third]    = Footer third
regions[footer_bottom]    = Footer bottom

; Stylesheets
stylesheets[all][] = css/layouts/simple2.layout.css
