<?php

/**
 * @file breadcrumb.theme.inc
 */

/**
 * Returns HTML for a breadcrumb trail.
 *
 * @param $variables
 *   An associative array containing:
 *   - breadcrumb: An array containing the breadcrumb links.
 */
function d2d_breadcrumb($variables)
{
  $breadcrumb = $variables['breadcrumb'];

  if(arg(0) == 'node' && !drupal_is_front_page()) {
    $node = node_load(arg(1));
    $breadcrumb[] = $node->title;
  }

  //about us page
  if (arg(1) == '108') {
      $breadcrumb[1] = 'About Us';
  }

  //Reviews page
  if (arg(0) == 'what-our-clients-say') {
    $breadcrumb[] = 'What Our Clients Say';
  }

  if (arg(0) == 'faq-page') {
    $breadcrumb[] = 'Frequently asked questions';
  }

  if (arg(0) == 'blog') {
    $breadcrumb[] = 'Blog';
  }

    //Gets rid of team-member/all in the breadcrumb.
  if (arg(0) == 'about-us') {
    $breadcrumb[1] = '<a href="/about-us">About Us</a>';
      $teamName = arg(1);
      $breadcrumb[2] = ucfirst($teamName);
  }

    if (arg(0) == 'holidays') {
        $breadcrumb[1] = 'Holidays';
    }


  //Blog
    $type = arg(0);
    $taxType = arg(1);

    $node = node_load(arg(1));
    $nodeType = $node->type;

    if($nodeType == "blog_posting"){

        //Find the blog category by searching the three parameters. node type, entity, and field name (machine name)
        $blog_category = field_get_items('node', $node, 'field_blog_category');

        //give new variable the value of the blog category ID
        $tid = $blog_category[0]['tid'];

        //Load the tax term id.
        $cat_term = taxonomy_term_load($tid);

        //"convert" tax term id to tax name.
        $tax = $cat_term->name;

        $breadcrumb[1] = '<a href="/blog">' . 'Blog' . '</a>';
        $tax2 = str_replace(" ","-",$tax);
        $taxlower = strtolower($tax2);
        $breadcrumb[2] = '<a href="../../../category/' . $taxlower . '">' . ucfirst($tax) .'</a>' ;
        $breadcrumb[3] = $node->title;
    }

    $blogcategory = arg(2);

    if($taxType == 'category'){
        $termid = arg(2);
        $termname = taxonomy_term_load($termid);
        $blogcategory = $termname->name;
        $blogcategory2 = str_replace("-"," ",$blogcategory);
        $blogcategory3 = ucwords($blogcategory2);
        $breadcrumb[1] = '<a href="/blog">' . 'Blog' . '</a>';
        $breadcrumb[2] = $blogcategory3;
    }


  if (!empty($breadcrumb)) {
// Provide a navigational heading to give context for breadcrumb links to
// screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb">' . implode(' / ', $breadcrumb) . '</div>';
    return $output;
  }
}